#!/usr/bin/python3

from vautour import Vautour
from termcolor import colored

vau = Vautour()

while not vau.fini():
    print(colored("===============", "blue"))
    print(colored("Votre main: %s" % vau.cartes_joueur, "blue"))
    print(colored("===============", "blue"))
    choixPasBon = True
    while choixPasBon:
        err_entree = False
        while not err_entree:
            try:
                choix = int(input(colored("Votre choix: ", "green")))
                err_entree = True
            except ValueError:
                print("Ce n'est pas un nombre !")
        if choix in vau.cartes_joueur:
            choixPasBon = False
        else:
            print(colored("Vous n'avez plus ou pas cette carte !", "red"))
    print(colored("Choix de l'ennemi: %d" % vau.carte_ennemi, "grey"))
    print(colored("Pioche: %s" % vau.get_animal(), "blue"))
    vau.gain(choix)
    vau.update()

print()
print("===Vous: ===")
print("Souris: %s" % vau.souris_joueur)
print("Vautour: %s" % vau.vautour_joueur)
print("=============")
print("===Ennemi: ===")
print("Souris: %s" % vau.souris_ennemi)
print("Vautour: %s" % vau.vautour_ennemi)
print()
print("Résultats: ")
print("Vous: %d points" % vau.get_resultat()[0])
print("Ennemi: %d points" % vau.get_resultat()[1])
vau.gagnant()
