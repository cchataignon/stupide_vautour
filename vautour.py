#!/usr/bin/python3

import random
from termcolor import colored


class Vautour:

    def __init__(self):
        self.cartes_ennemi = list(range(1, 16))
        self.cartes_joueur = list(range(1, 16))
        self.souris_joueur = []
        self.souris_ennemi = []
        self.vautour_joueur = []
        self.vautour_ennemi = []
        self.souris_vautour = list(range(-5, 11))
        self.souris_vautour.remove(0)
        self.update()

    def update(self):
        if len(self.souris_vautour) > 0:
            self.choix = random.choice(self.souris_vautour)
            self.carte_ennemi = random.choice(self.cartes_ennemi)

    def get_animal(self):
        return "Vautour" if self.choix < 0 else "Souris"

    def gain(self, carte_joueur):
        animal = self.get_animal()

        self.cartes_ennemi.remove(self.carte_ennemi)
        self.cartes_joueur.remove(carte_joueur)
        self.souris_vautour.remove(self.choix)

        if carte_joueur == self.carte_ennemi:
            print("Egalité. Personne n'a rien gagné !")
        elif animal == "Souris":
            if carte_joueur > self.carte_ennemi:
                self.souris_joueur.append(self.choix)
                print(colored("Vous avez gagné une souris !", "green"))
            else:
                print(colored("Vous n'avez pas gagné la souris !", "red"))
                self.souris_ennemi.append(self.choix)
        elif animal == 'Vautour':
            if carte_joueur < self.carte_ennemi:
                self.vautour_joueur.append(self.choix)
                print(colored("Vous avez gagné un vautour !", "red"))
            else:
                print(colored("Vous n'avez pas gagné le vautour !", "green"))
                self.vautour_ennemi.append(self.choix)
        else:
            print('Erreur: pas de souris, ni de vautour !')

    def fini(self):
        return self.souris_vautour == [] or self.cartes_ennemi == [] or self.cartes_joueur == []

    def get_resultat(self):
        return (sum(self.vautour_joueur) + sum(self.souris_joueur), sum(self.vautour_ennemi) + sum(self.souris_ennemi))

    def gagnant(self):
        res = get_resultat()
        if res[0] > res[1]:
            print("Vous avez gagné !")
        else:
            print("Vous avez perdu !")
